# Et radikalt forslag for sikring av dine personopplysninger

av Richard Stallman, 2018-04-03

**Overvåkningen som tvinges på oss i dag er verre enn i Sovjetunionen.
Vi trenger lover for å hindre at disse opplysningene i det hele tatt samles
inn.**

Journalister har spurt meg om de negative reaksjonene på misbruk av
[Facebook](https://www.theguardian.com/technology/2018/mar/31/big-data-lie-exposed-simply-blaming-facebook-wont-fix-reclaim-private-information)-data
kan være et vendepunkt for kampanjen for å gjenvinne vernet av
privatsfæren.  Det er mulig, hvis befolkningen gjør kampanjen bredere og mer
dyptgående.

Bredere, ved å utvide den til å gjelde alle overvåkningssystemer, ikke bare
[Facebook](https://www.theguardian.com/technology/facebook). Dypere, ved å
komme seg fra regulering av hvordan opplysningene brukes, til å regulere
innsamling av opplysninger. Dette fordi overvåkningen er så gjennomgående at
det trengs store endringer og kraftige tiltak for å gjenvinne vern av
privatsfæren.

Overvåkningen vi utsettes for i dag er langt mer omfattende enn
overvåkningen i Sovjetunionen. Vi må fjerne den store brorparten av
overvåkningen for demokratiet og frihetens skyld. Det er så mange måter å
bruke opplysninger til å skade mennesker på, at den eneste trygge databasen
er den som aldri ble opprettet. Dermed, i stedet for EUs tilnærming som
hovedsakelig regulerer hvordan personopplysninger kan brukes (se
[GDPR](https://www.eugdpr.org/)), foreslår jeg en lov som nekter systemer å
samle inn personopplysninger.

Den robuste måten å gjøre det på, måten som sikrer at en ikke er prisgitt
innfallene til nåværende eller fremtidige myndigheter, er å kreve at
systemer bygges slik at det ikke samler inn informasjon om en person. Det
grunnleggende prinsippet er at et system må utformes til å ikke samle inn en
bestemt type opplysninger, hvis systemets grunnleggende funksjon kan fungere
uten slike opplysninger.

Opplysninger om hvem som reiser hvor er spesielt sensitivt, da det er et
utmerket grunnlag for å undertrykke ethvert utvalgt mål. Vi kan bruke
Londons tog og busser som et eksempel.

Det digitale betalingskortsystemet til kollektivtransportsystemet i London
lagrer sentralt for hver tur hvilket reisekort eller bankkort som er brukt
til å betale for reisen. Når en passasjer fyller opp reisekortet digitalt,
knyttes reisekort til passasjerens identitet. I sum gir dette komplett
overvåkning.

Jeg antar at kollektivtransportsystemet kan forsvare denne praksisen i tråd
med GDPR-reglene. Mitt forslag, derimot, vil kreve at systemet slutter å
spore hvem som drar hvor. Reisekortets grunnleggende funksjon er å betale
for transport. Dette kan gjøres uten å sentralisere disse opplysningene, så
transportsystemet må dermed slutte med dette. Når det aksepteres digitale
betalinger, bør det gå igjennom et anonymt betalingssystem.

Ekstrafunksjoner i systemet, som å gi en passasjer mulighet til å se sine
tidligere reiser, er ikke del av de grunnleggende funksjonene. Slike
ekstrafunksjoner kan ikke forsvare ytterligere overvåkning.

Disse tilleggstjenestene kan tilbys separat for brukere som ber om
dem. Eller enda bedre, brukere kan bruke sine egne personlige systemer til å
holde styr på sine egne reiser privat.

Vanlige drosjer demonstrerer at et system for å leie biler med sjåfør, ikke
trenger identiteten til passasjerene. Dermed bør ikke slike systemer ha lov
til å identifisere passasjerer; det bør kreves at de godtar
privatlivsbevarende kontanter fra passasjerer uten i det hele tatt å forsøke
å identifisere dem.

Imidlertid kan passende digitale betalingssystemer også beskytte
passasjerers anonymitet og personvern. Vi har allerede utviklet et: [GNU
Taler](https://taler.net/en/). Det er designet for å være anonymt for den
som betaler, mens betalingsmottakere alltid identifiseres. Vi utformet det
slik for å hindre skatteunndragelse. Alle digitale betalingssystemer bør
pålegges å sikre anonymitet ved å bruke denne eller tilsvarende metode.

Hva med sikkerheten? Systemer i områder der publikum kan ferdes må være
utformet slik at de ikke kan spore folk. Videokameraer bør lage en lokal
innspilling som kan kontrolleres de kommende ukene om en forbrytelse begås,
men bør ikke tillate ekstern visning uten at man fysisk må hente
innspillingene. Biometriske systemer bør være utformet slik at de bare
gjenkjenner personer fra en rettslig bestemt liste over mistenkte, og slik
at personvernet for oss andre respekteres. En urettferdig stat er farligere
enn terrorisme, og for mye sikkerhet oppmuntrer til en urettferdig stat.

Intensjonene til EUs GDPR-reguleringene er gode, men går ikke langt
nok. Reguleringene vil ikke hegne om privatsfæren, da regelverket i den er
for slapt. Der tillates innsamling av hvilke som helst opplysninger som på
en eller annen måte kan være nyttig for et system, og det er enkelt å komme
opp med måter å gjøre hvilke som helst opplysninger nyttig for et eller
annet.

GDPR legger (i noen tilfeller) mye i å kreve brukeres samtykke til
innsamling av opplysninger om dem, men det er ikke mye til
hjelp. Systemdesignere har blitt eksperter på å fabrikere samtykke
(«Manufacturing Consent», for å gjenbruke Noam Chomskys uttrykk). De fleste
brukere godtar vilkårene til et nettsted uten å lese dem. Et selskap som
[krevde](https://www.theguardian.com/technology/2014/sep/29/londoners-wi-fi-security-herod-clause)
brukere å avstå sitt førstefødte barn, fikk samtykke fra mange brukere. Det
er slik, at når et system er avgjørende for moderne liv, som busser og tog,
ignorerer brukere vilkårene, da det å nekte samtykke er for krevende å
vurdere.

For å få tilbake vern av privatsfæren må vi stoppe overvåkningen før det i
det hele tatt blir spørsmål om samtykke.

Til slutt, glem ikke programvaren på din egen datamaskin. Hvis det er
produsenteid programvare fra Apple, Google eller Microsoft, så [spionerer
den på deg regelmessig](https://gnu.org/malware/). Det er fordi den er
kontrollert av et selskap som ikke nøler med å spionere på deg. Selskaper
har en tendens til å miste sine skrupler når det er lønnsomt. Derimot er fri
programvare [kontrollert av dens
brukere](https://gnu.org/philosophy/free-software-even-more-important.html).
Brukerfellesskapet sikrer at programvaren forblir ærlig.

Richard Stallman er leder av Free Software Foundation, som tok initiativet
til friprog-operativsystemet GNU.

© Richard Stallman 2018. Oversettelsen er © Petter Reinholdtsen
mfl. 2018. Utgitt med [Creative Commons Attribution NoDerivatives lisens
4.0](https://creativecommons.org/licenses/by-nd/4.0/).  Den originale
engelske versjonen ble opprinnelig publisert i [The
Guardian](https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance)
2018-04-03.
