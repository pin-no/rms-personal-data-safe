# Kişisel verilerinizi güvende tutmak için radikal bir öneri

Richard Stallman tarafından, 3 Nisan 2018

**Bugün üzerimizde uygulanan gözetim Sovyetler Birliği'ndekinden daha
kötü durumda. Bu verilerin toplanmasını en baştan durdurmak için
kanunlara ihtiyacımız var.**

Gazeteciler bana
[Facebook](https://www.theguardian.com/technology/2018/mar/31/big-data-lie-exposed-simply-blaming-facebook-wont-fix-reclaim-private-information)
verilerinin kötüye kullanılmasına gösterilen tepkinin mahremiyetin  geri
kazanılması kampanyasında bir dönüm noktası olup olamayacağını
soruyor. Olabilir, eğer insanlar kampanyalarını daha kapsamlı ve derin bir
hale getirebilirlerse.

Daha kapsamlı, sadece
[Facebook](https://www.theguardian.com/technology/facebook) değil tüm
gözetleme sistemlerini dahil etmek anlamında. Daha derin, verilerin
kullanımını denetlemekten verilerin toplanmasını denetlemeye geçmek
anlamında. Gözetleme her tarafa yayılmış olduğu için, mahremiyeti eski
haline getirmek muhakkak büyük bir değişiklik olacaktır, ve güçlü önlemler
almayı gerektirmektedir.

Bugün üzerimizde uygulanan gözetim Sovyetler Birliği'ndekinden çok daha kötü
durumda. Özgürlük ve demokrasi için, bunun büyük kısmını ortadan kaldırmak
zorundayız. İnsanlara zarar vermek için verileri kullanmanın birçok yolu
var, bu yüzden en güvenli veri tabanı hiç toplanmamış olandır. Bundan
dolayı, AB'nin genel olarak kişisel verilerin nasıl kullanılacağını
düzenleyen yaklaşımı yerine ([Genel Veri Koruma
Yönetmeliği](https://www.eugdpr.org/)  veya GDPR), ben sistemlerin kişisel
verileri toplamasını durduran bir kanun öneriyorum.

Bunu yapmanın en sağlam yolu, bir hükümetin isteğiyle kenara
kaldırılamayacak bir yol, sistemlerin bir kişi hakkında veri toplamayacak
şekilde kurulmasını zorunlu kılmaktır. Temel prensip, bir sistem temel
işlevini veri olmadan yerine getirebiliyorsa o sistemin veri toplamayacak
şekilde tasarlanmasıdır.

Kimin nereye seyahat ettiği özellikle çok hassas bir bilgidir, çünkü
herhangi bir seçilmiş hedefi baskılamak için ideal bir dayanaktır. Örnek
olarak Londra'daki trenleri ve otobüsleri alabiliriz.

Transport for London dijital ödeme kartı sistemi, herhangi bir Oyster ya da
banka kartıyla ödemesi yapılan bütün yolculukları merkezi bir şekilde kayıt
altına almaktadır. Bir yolcu kartı dijital olarak doldurduğunda, sistem
kartı yolcunun kimliğiyle ilişkilendirmektedir. Bu tamamıyla bir gözetim
anlamına gelmektedir.

Ulaşım sisteminin bu durumu GDPR kuralları altında haklı gösterebileceğini
düşünüyorum. Benim teklifim, aksine, sistemin kimin nereye gittiğini takip
etmeyi bırakmasıdır. Kartın temel işlevi yolculuk için ödeme yapmaktır. Bu,
verileri merkezileştirmeden yapılabilir, bu yüzden ulaşım sistemi bunu
yapmaktan vazgeçmelidir. Dijital ödemeleri kabul ederken, bunu anonim bir
ödeme sistemi aracılığıyla yapmalıdır.

Sistemin süslü özellikleri, bir yolcunun geçmiş yolculuklarının listesini
görüntülemesine izin vermesi gibi, temel işlevin bir parçası değildir, bu
yüzden herhangi bir ilave gözetim yapılmasını haklı çıkarmaz.

Bu ilave hizmetler, talep eden kullanıcılara ayrı olarak sunulabilir. Daha
da iyisi, kullanıcılar yolculuklarını özel olarak takip etmek için kendi
kişisel sistemlerini kullanabilirler.

Siyah taksiler, sürücülü araba kiralamak için bir sistemin yolcuların
kimliğini belirlemesine gerek olmadığını göstermektedir. Bu yüzden bu gibi
sistemlerin yolcuların kimliğini belirlemesine izin verilmemelidir;
yolculardan kimliklerini belirlemeye çalışmadan mahremiyete saygı duyan
nakit para kabul etmeleri zorunlu kılınmalıdır.

Buna rağmen, kullanışlı dijital ödeme sistemleri de yolcuların gizliliğini
ve mahremiyetini koruyabilir. Biz zaten bir tane geliştirdik: [GNU
Taler](https://taler.net/en/). Ödemeyi yapanın anonim olması için
tasarlanmıştır, fakat ödemeyi alanlar her zaman bellidir. Vergi kaçırmaya
olanak sağlamaması için bu şekilde tasarladık. Bütün dijital ödeme
sistemlerinin bu veya benzer bir metodu kullanarak gizliliği koruması
zorunlu kılınmalıdır.

Peki ya güvenlik? Halka açık alanlardaki bu gibi sistemler insanları takip
etmeyecek şekilde tasarlanmalıdır. Video kameralar, bir suç meydana gelmesi
durumunda geriye dönük birkaç hafta boyunca kontrol edilebilecek yerel
kayıtlar yapmalıdır, fakat bu kayıtların fiziksel olarak alınmadan uzaktan
görüntülenmesine izin vermemelidir. Biyometrik sistemler, diğer herkesin
mahremiyetine saygı göstermesi için, sadece mahkeme emri ile şüpheliler
listesindeki insanları teshiş edecek şekilde tasarlanmalıdır. Adaletin
olmadığı bir ülke terörizmden daha tehlikelidir, ve çok fazla güvenlik
adaletsiz bir ülkeyi teşvik eder.

AB'nin GDPR düzenlemeleri iyi niyetlidir, fakat çok fazla ileriye
gitmemektedir. Kuralları çok gevşek olduğu için çok fazla mahremiyet
sağlamayacaktır. Eğer sisteme bir şekilde faydası varsa veri toplamaya izin
vermektedir, ve herhangi bir verinin birşeyler için faydalı olması için bir
yol bulmak çok kolaydır.

GDPR, kullanıcıların (bazı durumlarda) verilerinin toplanmasına onay
vermesini gerektirmektedir, fakat bu pek işe yaramamaktadır. Sistem
tasarımcıları onay almakta uzman olmuşlardır (Noam Chomsky’nin
ifadesiyle). Çoğu kullanıcı bir sitenin şartlarını okumadan onaylar;
kullanıcılardan karşılığında ilk doğan çocuklarını vermelerini  [zorunlu
kılan](https://www.theguardian.com/technology/2014/sep/29/londoners-wi-fi-security-herod-clause)
bir site birçok kullanıcıdan onay almıştır. Ve aynı şekilde, bir sistem
modern hayat için çok önemliyse, otobüsler ve trenler gibi, kullanıcılar
şartları görmezden gelmektedir çünkü onay vermemek düşünülmeyecek kadar can
sıkıcı olmaktadır.

Mahremiyeti geri kazanmak için, onay bile istemeden önce gözetlemeye bir son
vermeliyiz.

Son olarak, kendi bilgisayarınızdaki yazılımı unutmayın. Eğer Apple, Google
ya da Microsoft'un özgür olmayan yazılımı ise, [sizin hakkınızda düzenli
olarak casusluk
yapmaktadır](https://www.gnu.org/proprietary/proprietary.tr.html). Bunun
nedeni sizin hakkınızda casusluk yapmaktan çekinmeyecek şirketler tarafından
kontrol edilmeleridir. Kar söz konusu olduğunda şirketler ahlaki ilkelerini
kaybetme eğilimi göstermektedirler. Tam aksine, özgür yazılım [kullanıcıları
tarafından kontrol
edilmektedir](https://www.gnu.org/philosophy/free-software-even-more-important.tr.html).
Bu kullanıcı topluluğu yazılımın dürüst olarak kalmasını sağlamaktadır.

Richard Stallman, özgür işletim sistemi GNU'nun geliştirilmesini başlatan,
Özgür Yazılım Vakfı'nın başkanıdır.

Telif Hakkı 2018 Richard Stallman. [Creative Commons Atıf-Türetilemez
Lisansı 4.0](https://creativecommons.org/licenses/by-nd/4.0/deed.tr) altında
yayınlanmıştır.  Orijinal İngilizce versiyonu [The
Guardian](https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance)
adresinde yayınlanmıştır.  3 Nisan 2018.
