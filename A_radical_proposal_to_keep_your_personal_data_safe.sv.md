# Ett radikalt förslag för att säkra dina personliga data

av Richard Stallman, 2018-04-03

**Övervakningen som idag påtvingas oss är värre än i Sovjetunionen.
Vi behöver lagar som förhindrar att dessa data samlas in från
första början.**

Journalister har frågat mig om avskyn mot missbruket av
[Facebook](https://www.theguardian.com/technology/2018/mar/31/big-data-lie-exposed-simply-blaming-facebook-wont-fix-reclaim-private-information)-data
kan vara en vändpunkt för kampanjen om att återvinna skyddet för den privata
sfären. Det skulle kunna hända, om allmänheten gör kampanjen bredare och
djupare.

Bredare i betydelsen att utöka det till alla övervakningssystem, inte bara
[Facebook](https://www.theguardian.com/technology/facebook). Djupare, i
betydelsen att gå vidare från att reglera dataanvändning till att reglera
insamlandet av data. Eftersom övervakningen är så genomträngande så är ett
återerövrande av den privata sfären nödvändigtvis en stor förändring, och
kräver kraftfulla åtgärder.

Övervakningen som påtvingas oss idag överträffar vida den i
Sovjetunionen. För frihetens och demokratins skull behöver vi göra oss av
med det mesta av den. Det finns så många sätt att använda data för att skada
människor att den enda säkra databasen är den som aldrig samlades in. Därför
föreslår jag, snarare än EU:s angreppssätt om att främst reglera hur
personliga data kan användas (i dess [General Data Protection
Regulation](https://www.eugdpr.org/)  eller GDPR), istället en lag för att
stoppa system från att samla in personliga data.

Det robusta sättet att göra det, sättet som inte kan åsidosättas så fort en
regering känner för det, är att kräva att system byggs så att de inte samlar
in data om en person. Grundprincipen är att ett system måste konstrueras så
att det inte samlar in vissa data, om dess grundfunktion kan utföras utan
dessa data.

Data om vem som reser vart är särskilt känslig, för det är en idealisk grund
för att förtrycka ett givet mål. Vi kan ta Londons tåg och bussar som en
fallstudie.

Det digitala betalningskortsystemet som ”Transport for London” har lagrar
centralt resorna som ett givet Oyster- eller bankkort har betalat. Då en
passagerare laddar kortet digitalt associerar systemet kortet med
passagerarens identitet. Summan av detta blir total övervakning.

Jag förväntar mig att transportsystemet kan rättfärdiga denna utövning under
GDPR-reglerna. Mitt förslag skulle i motsats till detta kräva att systemet
slutar spåra vem som åker vart. Kortets grundläggande funktion är att betala
för transport. Det kan göras utan att centralisera dessa data, så
transportsystemet skulle behöva sluta göra det. När det accepterar digital
betalning bör det göra så genom ett anonymt betalningssystem.

Extrafunktioner i systemet, som att låta en passagerare se listan över
tidigare resor, är inte en del av grundfunktionaliteten, så de kan inte tas
som ett försvar till införandet av ytterligare övervakning.

Dessa extratjänster skulle kunna erbjudas separat till användare som
efterfrågar dem. Eller ännu bättre, användare skulle kunna använda sina egna
personliga system för att spåra sina egna resor.

Taxibilar demonstrerar att ett system för att hyra bilar med förare inte
behöver identifiera passagerare. Sådana system ska därför inte få
identifiera förare, de bör vara tvungna att acceptera integritetsskyddande
kontanter från passagerare utan att någonsin försöka identifiera dem.

Bekväma digitala betalningssystem kan dock också skydda användares
anonymitet och integritet. Vi har redan utvecklat ett: [GNU
Taler](https://taler.net/en/). Det är designat för att vara anonymt för
betalaren, men de som tar emot betalningen identifieras alltid. Vi designade
det på det sättet för att inte underlätta skattesmitning. Alla digitala
betalningssystem bör ha kravet att skydda anonymitet med den här metoden
eller någon liknande.

Hur ser säkerheten ut? Sådana system i områden där allmänheten har tillträde
måste designas så att de inte kan spåra personer. Videokameror bör göra en
lokal inspelning som kan kontrolleras under de närmaste veckorna om ett
brott inträffar, men bör inte tillåta fjärrvisning utan fysisk insamling av
inspelningen. Biometriska system bör designas så att de endast känner igen
personer på en lista med misstänkta efter domstolsbeslut, för att respektera
integriteten hos resten av oss. En orättvis stat är farligare än terrorism,
och för mycket säkerhet uppmuntrar en orättvis stat.

EU:s GDPR-regler är välmenande, men sträcker sig inte särskilt långt. De ger
inte mycket integritet eftersom reglerna är för tillåtande. De tillåter
insamling av alla sorters data om det på något sätt är användbart för
systemet, och det är enkelt att komma på ett sätt att få några särskilda
data att vara användbara för något.

GDPR håller mycket på att kräva att användare (i vissa fall) ger medgivande
till insamlingen av deras data, men det gör inte mycket
nytta. Systemdesigners har blivit experter på att tillverka medgivande (för
att återanvända Noam Chomskys fras). De flesta användare godkänner en
webbplats villkor utan att läsa dem; ett företag som
[krävde](https://www.theguardian.com/technology/2014/sep/29/londoners-wi-fi-security-herod-clause)
att användare skulle byta bort sitt förstfödda barn fick medgivande från
många användare. Men användare ignorerar även villkoren då ett system är
avgörande i det moderna livet, som bussar och tåg, eftersom att vägra att ge
medgivande är för plågsamt för att ens överväga.

För att återställa integriteten måste vi stoppa övervakning innan den ens
ber om samtycke.

Glöm slutligen inte programvaran i din egen dator. Om det är den ofria
programvaran från Apple, Google eller Microsoft så [spionerar den på dig
regelbundet](https://gnu.org/malware/). Detta är för att den kontrolleras av
ett företag som inte kommer att tveka inför att spionera på dig. Företag har
en tendens att förlora sina skrupler då det är lönsamt. Som en kontrast
kontrolleras fri programvara [av sina
användare](https://gnu.org/philosophy/free-software-even-more-important.html).
Den användargemenskapen gör att programvaran förblir ärlig.

Richard Stallman är president för Free Software Foundation, som startade
utvecklingen av ett fritt operativsystem, GNU.

Copyright 2018 Richard Stallman. Släppt under [Creative Commons Attribution
NoDerivatives License
4.0](https://creativecommons.org/licenses/by-nd/4.0/).  Den ursprungliga
engelska versionen publicerad i [The
Guardian](https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance)
2018-04-03.
