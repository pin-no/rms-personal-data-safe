Translation of 'A radical proposal to keep your personal data safe'.

The text is copyright 2018 by Richard Stallman and released under
[Creative Commons Attribution NoDerivatives License
4.0](https://creativecommons.org/licenses/by-nd/4.0/).  The text was
first publised in [The
Guardian](https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance),
and is translated with approval from Richard Stallman and The Guardian.

This source is available from a git repository on
[Gitlab](https://gitlab.com/pin-no/rms-personal-data-safe).  The
translation is hosted by
[Weblate](https://hosted.weblate.org/projects/rms-personal-data-safe/).

HTML editions are available from
[Gitlab pages](https://pin-no.gitlab.io/rms-personal-data-safe/).
