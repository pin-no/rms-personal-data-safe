# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Richard Stallman
# This file is distributed under the same license as the A radical proposal to keep your personal data safe package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: A radical proposal to keep your personal data safe n/a\n"
"POT-Creation-Date: 2020-10-28 06:25+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title #
#: A_radical_proposal_to_keep_your_personal_data_safe.md:1
#, no-wrap
msgid "A radical proposal to keep your personal data safe"
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:4
msgid "by Richard Stallman, 2018-04-03"
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:8
#, no-wrap
msgid ""
"**The surveillance imposed on us today is worse than in the Soviet\n"
"Union. We need laws to stop this data being collected in the first\n"
"place.**\n"
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:15
msgid ""
"Journalists have been asking me whether the revulsion against the abuse of "
"[Facebook](https://www.theguardian.com/technology/2018/mar/31/big-data-lie-exposed-simply-blaming-facebook-wont-fix-reclaim-private-information)  "
"data could be a turning point for the campaign to recover privacy. That "
"could happen, if the public makes its campaign broader and deeper."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:21
msgid ""
"Broader, meaning extending to all surveillance systems, not just "
"[Facebook](https://www.theguardian.com/technology/facebook). Deeper, meaning "
"to advance from regulating the use of data to regulating the accumulation of "
"data. Because surveillance is so pervasive, restoring privacy is necessarily "
"a big change, and requires powerful measures."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:30
msgid ""
"The surveillance imposed on us today far exceeds that of the Soviet "
"Union. For freedom and democracy’s sake, we need to eliminate most of "
"it. There are so many ways to use data to hurt people that the only safe "
"database is the one that was never collected. Thus, instead of the EU’s "
"approach of mainly regulating how personal data may be used (in its [General "
"Data Protection Regulation](https://www.eugdpr.org/)  or GDPR), I propose a "
"law to stop systems from collecting personal data."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:36
msgid ""
"The robust way to do that, the way that can’t be set aside at the whim of a "
"government, is to require systems to be built so as not to collect data "
"about a person. The basic principle is that a system must be designed not to "
"collect certain data, if its basic function can be carried out without that "
"data."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:40
msgid ""
"Data about who travels where is particularly sensitive, because it is an "
"ideal basis for repressing any chosen target. We can take the London trains "
"and buses as a case for study."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:45
msgid ""
"The Transport for London digital payment card system centrally records the "
"trips any given Oyster or bank card has paid for. When a passenger feeds the "
"card digitally, the system associates the card with the passenger’s "
"identity. This adds up to complete surveillance."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:52
msgid ""
"I expect the transport system can justify this practice under the GDPR’s "
"rules. My proposal, by contrast, would require the system to stop tracking "
"who goes where. The card’s basic function is to pay for transport. That can "
"be done without centralising that data, so the transport system would have "
"to stop doing so. When it accepts digital payments, it should do so through "
"an anonymous payment system."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:56
msgid ""
"Frills on the system, such as the feature of letting a passenger review the "
"list of past journeys, are not part of the basic function, so they can’t "
"justify incorporating any additional surveillance."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:60
msgid ""
"These additional services could be offered separately to users who request "
"them. Even better, users could use their own personal systems to privately "
"track their own journeys."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:66
msgid ""
"Black cabs demonstrate that a system for hiring cars with drivers does not "
"need to identify passengers. Therefore such systems should not be allowed to "
"identify passengers; they should be required to accept privacy-respecting "
"cash from passengers without ever trying to identify them."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:73
msgid ""
"However, convenient digital payment systems can also protect passengers’ "
"anonymity and privacy. We have already developed one: [GNU "
"Taler](https://taler.net/en/). It is designed to be anonymous for the payer, "
"but payees are always identified. We designed it that way so as not to "
"facilitate tax dodging. All digital payment systems should be required to "
"defend anonymity using this or a similar method."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:83
msgid ""
"What about security? Such systems in areas where the public are admitted "
"must be designed so they cannot track people. Video cameras should make a "
"local recording that can be checked for the next few weeks if a crime "
"occurs, but should not allow remote viewing without physical collection of "
"the recording. Biometric systems should be designed so they only recognise "
"people on a court-ordered list of suspects, to respect the privacy of the "
"rest of us. An unjust state is more dangerous than terrorism, and too much "
"security encourages an unjust state."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:89
msgid ""
"The EU’s GDPR regulations are well-meaning, but do not go very far. It will "
"not deliver much privacy, because its rules are too lax. They permit "
"collecting any data if it is somehow useful to the system, and it is easy to "
"come up with a way to make any particular data useful for something."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:100
msgid ""
"The GDPR makes much of requiring users (in some cases) to give consent for "
"the collection of their data, but that doesn’t do much good. System "
"designers have become expert at manufacturing consent (to repurpose Noam "
"Chomsky’s phrase). Most users consent to a site’s terms without reading "
"them; a company that "
"[required](https://www.theguardian.com/technology/2014/sep/29/londoners-wi-fi-security-herod-clause)  "
"users to trade their first-born child got consent from plenty of users. Then "
"again, when a system is crucial for modern life, like buses and trains, "
"users ignore the terms because refusal of consent is too painful to "
"consider."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:103
msgid ""
"To restore privacy, we must stop surveillance before it even asks for "
"consent."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:112
msgid ""
"Finally, don’t forget the software in your own computer. If it is the "
"non-free software of Apple, Google or Microsoft, it [spies on you "
"regularly](https://gnu.org/malware/). That’s because it is controlled by a "
"company that won’t hesitate to spy on you. Companies tend to lose their "
"scruples when that is profitable. By contrast, free (libre)  software is "
"[controlled by its "
"users](https://gnu.org/philosophy/free-software-even-more-important.html). "
"That user community keeps the software honest."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:115
msgid ""
"Richard Stallman is president of the Free Software Foundation, which "
"launched the development of a free/libre operating system GNU."
msgstr ""

#. type: Plain text
#: A_radical_proposal_to_keep_your_personal_data_safe.md:121
msgid ""
"Copyright 2018 Richard Stallman. Released under [Creative Commons "
"Attribution NoDerivatives License "
"4.0](https://creativecommons.org/licenses/by-nd/4.0/).  The original English "
"version was published in [The "
"Guardian](https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance)  "
"2018-04-03."
msgstr ""
