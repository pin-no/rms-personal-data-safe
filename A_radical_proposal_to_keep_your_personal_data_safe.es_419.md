# Una solución radical para mantener seguros sus objetos personales

Por Richard Stallman, 2018-04-03

La vigilancia impuesta hoy en día es peor que la impuesta en la Unión Soviética. Para empezar, hacen falta leyes para deneter tal recolección de datos.

Me han preguntado varios periodistas si mi repulsión contra el abuso de
datos de
[Facebook](https://www.theguardian.com/technology/2018/mar/31/big-data-lie-exposed-simply-blaming-facebook-wont-fix-reclaim-private-information)
pudiera ser un punto de quiebre para la campaña para recuperar la
privacidad. Eso podría ocurrir, si el público hace que esta campaña sea más
amplia y más profundo.

Más amplia quiere decir, ampliarse para incluir a los distintos sistemas de
vigilancia, no únicamente a
[Facebook](https://www.theguardian.com/technology/facebook). Más profunda,
en el sentido de avanzar más allá de regular el uso de datos, y hacia
regular la acumulación de datos. Porque, dado que la vigilancia es tan
invasiva, recuperar la privacidad necesariamente será un cambio profundo, y
requiere de medidas poderosas.

La vigilancia que nos ha sido impuesta hoy en día excede con creces la que
existió en la Unión Soviética. En aras de la libertad y la democracia,
debemos eliminar la mayor parte de ésta. Hay una tan gran cantidad de
maneras en que los datos pueden ser utilizados para causar daño que la única
base de datos segura es una que nunca haya sido recopilada. Yo propongo por
tanto, en vez de un esquema como el de la Unión Europea que busca regular
cómo se pueden utilizar los datos personales (en su [Regulación General de
Protección de Datos, o GDPR](https://eugdpr.org/)), una ley que detenga a
los sistemas de recolección de datos personales.

La manera más robusta de lograrlo, una manera que no puede ser ignorada ante
un capricho del gobierno en turno, es requerir que los sistemas sean
construidos de manera que no recolecten los datos acerca de cada persona. El
principio básico es que un sistema debe ser diseñado para no recolectar
determinado dato siempre que su función pueda ser cumplida sin dichos datos.

Los datos acerca de los viajes de cada persona son particularmente
sensibles, porque constituyen una base ideal para reprimir a cualquier
objetivo elegido. Podemos tomar como ejemplo los trenes y autobuses de la
ciudad de Londres.

El sistema de las tarjetas de pago de Transportes de Londres registra
centralmente los viajes que cada una de las tarjetas ha pagado. Cuando el
pasajero carga digitalmente la tarjeta, el sistema asocia su identidad con
la tarjeta. Esto se suma y genera un resultado que permite la vigilancia
total.

Espero que el sistema pueda justificar esta práctica siguiendo las reglas de
la GDPR. Mi propuesta, en contraste, requeriría que el sistema deje de
registrar quién va a dónde. La función básica de la tarjeta es pagar por el
uso de infraestructura de transporte. Esto puede hacerse sin centralizar la
información, por lo que el sistema de transporte debería dejar de
hacerlo. Al aceptar cargas de saldo digitales, debería hacerlo sobre un
sistema anónimo de pago.

Los extras del sistema, como brindarle a un pasajero revisar la lista de
viajes que se han registrado a su nombre, no son parte de la función básica,
por lo que no pueden justificar incorporar medidas adicionales de
vigilancia.

Estos servicios adicionales podrían ofrecerse por separado a los usuarios
que los requieran. O, mejor aún, los usuarios podrían utilizar sus propios
sistemas personales para, de forma privada, dar seguimiento a sus propios
viajes.

Los taxis negros demuestran que un sistema para contratar autos con
conductor no requieren de un mecanismo de identificación de pasajeros. Por
tanto, no debería permitírsele a los sistemas que resuelvan esa necesidad
identificar a los pasajeros; debería requerírseles recibir efectivo, un
medio de pago que respeta a la privacidad, sin intentar siquiera
identificarlos.

Sin embargo, los convenientes sistemas digitales de pago también podrían
proteger el anonimato y privacidad de los pasajeros. Ya desarrollamos uno:
[GNU Taler](https://taler.net/en/). Está diseñado para ser anónimo en
relación a quien paga, pero siempre identifica al destinatario del pago. Lo
diseñamos de esta manera para no facilitar la evasión fiscal. Debería
requerirse un método como este o similar de todos los sistemas digitales de
pago.

¿Y qué acerca de la seguridad? Estos sistemas en espacios donde pueda entrar
el público deben ser diseñados de manera que no puedan rastrear a las
personas. Las cámaras de video deben generar grabaciones locales que puedan
ser consultadas por varias semanas en caso de servir como evidencia ante un
crimen, pero no deben permitir la consulta remota sin que haya una
recolección física de la grabación. Los sistemas biométricos deben diseñarse
para que sólo reconozcan a las personas de entre una lista de sospechosos
provista por la corte, para respetar la privacidad del resto de nosotros. Un
Estado injusto es más peligroso que el terrorismo, y demasiada seguridad
pública impulsa a un Estado injusto.

Las regulaciones GDPR de la Unión Europea tienen buenas intenciones, pero no
son lo suficientemente profundas. No resultarán en mucha privacidad, dado
que sus reglas son demasiado laxas. Permiten la recolección de datos siempre
que sean útiles al sistema, y es fácil encontrar justificación para reportar
que cualquier conjunto de datos es útil para algo.

La GDPR hace mucho énfasis en requerir que los usuarios (en determinados
casos) den consentimiento a la recolección de sus datos, pero eso no es
suficiente. Los diseñadores de sistemas se han convertido en expertos en la
fabricación de concentimiento, retomando y adecuando la frase de Noam
Chomsky. La mayor parte de los usuarios consienten a los términos requeridos
por un sitio sin siquiera leerlos; una compañía
[requería](https://www.theguardian.com/technology/2014/sep/29/londoners-wi-fi-security-herod-clause)
a los usuarios el entregar a su hijo primogénito, y obtuvo el consentimiento
de muchísimos usuarios. Por otro lado, si un sistema es crucial para
mantener una forma de vida moderna, como los trenes y autobuses, los
usuarios terminan ignorando los términos de dicho consentimiento, porque no
otorgar el consentimiento resulta demasiado doloroso para considerarlo
seriamente.

Para recuperar la privacidad, debemos detener la vigilancia antes siquiera
de que nos pida el consentimiento.

Finalmente, no olviden el software en sus propias computadoras. Si se trata
del software no-libre de Apple, Google o Microsoft, éste [los espía
regularmente](https://gnu.org/malware/). Esto es porque es controlado por
una compañía que no dudará en espiar. Las compañías tienden a perder sus
escrúpulos cuando le resula lucrativo. En contraste, el software libre está
[controlado por sus
usuarios](https://gnu.org/philosophy/free-software-even-more-important.html).
La comunidad de usuarios es la que mantiene honesto al software.

Richard Stallman es presidente de la Fundación de Software Libre, la cual
inició el desarrollo del sistema operativo libre GNU.

Copyright 2018 Richard Stallman. Liberado bajo la [licencia Creative Commons
Atribución-NoDerivadas
4.0](https://creativecommons.org/licenses/by-nd/4.0/). La versión original
en inglés se publicó en [The
Guardian](https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance)
2018-04-03.
