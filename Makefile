all: markdown

LANGS := $(shell ls po/*.po|rev|cut -d. -f2|rev)

MDS = $(foreach L,$(LANGS),A_radical_proposal_to_keep_your_personal_data_safe.$(L).md)
PDFS = $(foreach L,$(LANGS),A_radical_proposal_to_keep_your_personal_data_safe.$(L).pdf)
HTMLS = $(foreach L,$(LANGS),A_radical_proposal_to_keep_your_personal_data_safe.$(L).html)

markdown: $(MDS)

pdf: $(PDFS)
%.pdf: %.md
	pandoc --variable papersize=A4 --variable geometry=margin=2cm -t latex $^ -o $@

html: $(HTMLS)
.md.html:
	pandoc -o $@ $^

po/A_radical_proposal_to_keep_your_personal_data_safe.pot: A_radical_proposal_to_keep_your_personal_data_safe.md
	po4a-gettextize -f text -o markdown -m A_radical_proposal_to_keep_your_personal_data_safe.md \
	-M UTF-8 -L UTF-8 \
	--package-name "A radical proposal to keep your personal data safe" \
	--copyright-holder "Richard Stallman" \
	--package-version "n/a" \
	| sed 's/CHARSET/UTF-8/' > $@.new && mv $@.new $@

po/A_radical_proposal_to_keep_your_personal_data_safe.%.po: po/A_radical_proposal_to_keep_your_personal_data_safe.pot
	po4a --no-translations --msgmerge-opt --no-location po4a.cfg

A_radical_proposal_to_keep_your_personal_data_safe.de.md: po/A_radical_proposal_to_keep_your_personal_data_safe.de.po A_radical_proposal_to_keep_your_personal_data_safe.md
	po4a --translate-only A_radical_proposal_to_keep_your_personal_data_safe.de.md po4a.cfg

A_radical_proposal_to_keep_your_personal_data_safe.es_419.md: po/A_radical_proposal_to_keep_your_personal_data_safe.es_419.po A_radical_proposal_to_keep_your_personal_data_safe.md
	po4a --translate-only A_radical_proposal_to_keep_your_personal_data_safe.es_419.md po4a.cfg

A_radical_proposal_to_keep_your_personal_data_safe.fr.md: po/A_radical_proposal_to_keep_your_personal_data_safe.fr.po A_radical_proposal_to_keep_your_personal_data_safe.md
	po4a --translate-only A_radical_proposal_to_keep_your_personal_data_safe.fr.md po4a.cfg

A_radical_proposal_to_keep_your_personal_data_safe.nb.md: po/A_radical_proposal_to_keep_your_personal_data_safe.nb.po A_radical_proposal_to_keep_your_personal_data_safe.md
	po4a --translate-only A_radical_proposal_to_keep_your_personal_data_safe.nb.md po4a.cfg 

A_radical_proposal_to_keep_your_personal_data_safe.pl.md: po/A_radical_proposal_to_keep_your_personal_data_safe.pl.po A_radical_proposal_to_keep_your_personal_data_safe.md
	po4a --translate-only A_radical_proposal_to_keep_your_personal_data_safe.pl.md po4a.cfg

A_radical_proposal_to_keep_your_personal_data_safe.sv.md: po/A_radical_proposal_to_keep_your_personal_data_safe.sv.po A_radical_proposal_to_keep_your_personal_data_safe.md
	po4a --translate-only A_radical_proposal_to_keep_your_personal_data_safe.sv.md po4a.cfg

A_radical_proposal_to_keep_your_personal_data_safe.tr.md: po/A_radical_proposal_to_keep_your_personal_data_safe.tr.po A_radical_proposal_to_keep_your_personal_data_safe.md
	po4a --translate-only A_radical_proposal_to_keep_your_personal_data_safe.tr.md po4a.cfg

po/A_radical_proposal_to_keep_your_personal_data_safe.nn.po: po/A_radical_proposal_to_keep_your_personal_data_safe.nb.po
	~/src/debian/pology/pologyrun pomtrans -s nob -t nno -p .nn:.nb apertium $@

clean:
	$(RM) *~ po/*~
distclean: clean
	$(RM) $(PDFS) $(HTMLS)

stats:
	for f in po/*.po; do printf "$$f "; msgfmt --output /dev/null --statistics $$f; done

.SUFFIXES: .md .html .pdf
