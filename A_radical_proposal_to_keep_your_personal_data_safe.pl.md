# Radykalna propozycja w celu zapewnienia bezpieczeństwa danych osobowych

od Richard Stallman, 2018-04-03

**Nadzór narzucony nam dzisiaj jest gorszy niż w ZSRR.
Potrzebujemy przepisów, aby zatrzymać gromadzenie tych danych w pierwszej kolejności.**

Dziennikarze pytali mnie, czy wstręt przeciwko nadużyciom
[Facebook](https://www.theguardian.com/technology/2018/mar/31/big-data-lie-exposed-simply-blaming-facebook-wont-fix-reclaim-private-information)
dane mogą być punktem zwrotnym dla kampanii w celu odzyskania
prywatności. Może się to zdarzyć, jeśli społeczeństwo poszerzy swoją
kampanię.

Szerszy, obejmujący wszystkie systemy nadzoru, nie tylko
[Facebook](https://www.theguardian.com/technology/facebook). Głębiej, czyli
przejść od regulacji wykorzystania danych do regulacji gromadzenia
danych. Ponieważ nadzór jest tak wszechobecny, przywracanie prywatności jest
koniecznie dużą zmianą i wymaga potężnych środków.

Nadzór nałożony na nas dzisiaj znacznie przewyższa nadzór Związku
Radzieckiego. Dla wolności i demokracji musimy wyeliminować większość z
nich. Jest tak wiele sposobów wykorzystywania danych do ranienia ludzi, że
jedyną bezpieczną bazą danych jest ta, która nigdy nie została
zebrana. Dlatego zamiast podejścia UE polegającego głównie na regulowaniu
sposobu, w jaki dane osobowe mogą być wykorzystywane (w [Ogólnym
Rozporządzeniu o Ochronie Danych](https://www.eugdpr.org/) lub RODO),
proponuję ustawę, która powstrzyma systemy przed gromadzeniem danych
osobistych.

Solidnym sposobem na zrobienie tego, czego nie można odłożyć na kaprys
rządu, jest wymaganie budowy systemów, aby nie gromadzić danych o
osobie. Podstawową zasadą jest to, że system musi być tak zaprojektowany,
aby nie gromadził pewnych danych, jeśli jego podstawową funkcję można
wykonać bez tych danych.

Dane o tym, kto podróżuje, są szczególnie wrażliwe, ponieważ stanowią
idealną podstawę do represji dowolnego wybranego celu. Jako przykład możemy
wziąć pociągi i autobusy z Londynu.

System cyfrowej karty płatniczej Transport for London rejestruje centralnie
podróże, za które zapłaciła dana karta Oyster lub karta bankowa. Gdy pasażer
podaje kartę cyfrowo, system kojarzy kartę z tożsamością pasażera. Daje to
całkowitą kontrolę.

Oczekuję, że system transportu może uzasadnić tę praktykę zgodnie z
przepisami RODO. Natomiast moja propozycja wymagałaby od systemu
zaprzestania śledzenia, kto idzie dokąd. Podstawową funkcją karty jest
opłacenie transportu. Można to zrobić bez centralizacji tych danych, więc
system transportu musiałby przestać to robić. Kiedy akceptuje płatności
cyfrowe, powinien to robić za pośrednictwem anonimowego systemu płatności.

Dodatki w systemie, takie jak umożliwienie pasażerowi przejrzenia listy
poprzednich podróży, nie są częścią podstawowej funkcji, więc nie mogą
uzasadnić włączenia dodatkowego nadzoru.

Te dodatkowe usługi mogą być oferowane osobno użytkownikom, którzy o nie
proszą. Co więcej, użytkownicy mogą korzystać z własnych systemów
osobistych, aby prywatnie śledzić swoje podróże.

Czarne taksówki pokazują, że system wynajmu samochodów z kierowcami nie musi
identyfikować pasażerów. Dlatego takie systemy nie powinny mieć możliwości
identyfikacji pasażerów; powinni być zobowiązani do przyjmowania szanujących
prywatność środków pieniężnych od pasażerów bez próby ich zidentyfikowania.

Jednak wygodne cyfrowe systemy płatności mogą również chronić anonimowość i
prywatność pasażerów. Opracowaliśmy już jeden: [GNU
Taler](https://taler.net/en/). Został zaprojektowany jako anonimowy dla
płatnika, ale odbiorcy są zawsze identyfikowani. Zaprojektowaliśmy to w taki
sposób, aby nie ułatwiać unikania podatków. Wszystkie cyfrowe systemy
płatności powinny być zobowiązane do obrony anonimowości przy użyciu tej lub
podobnej metody.

Co z bezpieczeństwem? Takie systemy w obszarach, w których publiczność jest
przyjmowana, muszą być zaprojektowane tak, aby nie mogły śledzić
ludzi. Kamery wideo powinny wykonać lokalne nagranie, które można sprawdzić
przez kilka następnych tygodni, jeśli dojdzie do przestępstwa, ale nie
powinno umożliwiać zdalnego oglądania bez fizycznego pobrania
nagrania. Systemy biometryczne powinny być zaprojektowane tak, aby
rozpoznawały tylko osoby na liście podejrzanych nakazanej przez sąd, aby
szanować prywatność reszty z nas. Niesprawiedliwe państwo jest bardziej
niebezpieczne niż terroryzm, a zbyt duże bezpieczeństwo zachęca do
niesprawiedliwego państwa.

Unijne przepisy RODO mają dobre intencje, ale nie idą zbyt daleko. Nie
zapewni dużej prywatności, ponieważ jego zasady są zbyt luźne. Pozwalają one
gromadzić dowolne dane, jeśli są w jakiś sposób przydatne dla systemu, i
łatwo jest wymyślić sposób, aby jakieś dane były przydatne do czegoś.

RODO w dużej mierze wymaga od użytkowników (w niektórych przypadkach)
wyrażenia zgody na gromadzenie ich danych, ale to nie robi nic
dobrego. Projektanci systemów stali się ekspertami w zakresie zgody na
produkcję (aby zmienić przeznaczenie frazy Noama Chomsky'ego). Większość
użytkowników zgadza się na warunki witryny bez ich czytania; firma, która
[wymagała](https://www.theguardian.com/technology/2014/sep/29/londoners-wi-fi-security-herod-clause)
od użytkowników do handlu swoim pierworodnym, otrzymała zgodę od wielu
użytkowników. Z drugiej strony, gdy system ma kluczowe znaczenie dla
współczesnego życia, takiego jak autobusy i pociągi, użytkownicy ignorują
warunki, ponieważ odmowa wyrażenia zgody jest zbyt bolesna do rozważenia.

Aby przywrócić prywatność, musimy przerwać inwigilację, zanim jeszcze
poprosi o zgodę.

Wreszcie, nie zapomnij o oprogramowaniu na swoim komputerze. Jeśli jest to
komercyjne oprogramowanie Apple, Google lub Microsoft, [szpieguje cię
regularnie](https://gnu.org/malware/). To dlatego, że jest kontrolowany
przez firmę, która nie waha się ciebie szpiegować. Firmy zwykle tracą
skrupuły, gdy jest to opłacalne. Natomiast darmowe (libre) oprogramowanie
jest [kontrolowane przez jego
użytkowników](https://gnu.org/philosophy/free-software-even-more-important.html).
Społeczność użytkowników zapewnia uczciwość oprogramowania.

Richard Stallman jest prezesem Free Software Foundation, która
zapoczątkowała rozwój systemu operacyjnego GNU w wersji darmowej / wolnej.

Prawa autorskie 2018 Richard Stallman. Wydane na podstawie [Creative Commons
Attribution NoDerivatives License
4.0](https://creativecommons.org/licenses/by-nd/4.0/). Oryginalna wersja
angielska została opublikowana w [The
Guardian](https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance)
2018-04- 03
